package fr.afpa.tictactoe;

import javafx.scene.shape.Rectangle;

public class BoardSquare extends Rectangle {
    private boolean markX = false;
    private boolean markO = false;

    public BoardSquare() {
    }

    public BoardSquare(boolean markX, boolean markO) {
        this.markX = markX;
        this.markO = markO;
    }

    public void take(boolean markX) {
            this.markX = markX;
            this.markO = !markX;
    }

    public boolean hasMarkX() {
        return this.markX;
    }

    public boolean hasMarkO() {
        return this.markO;
    }

    public boolean isTaken() {
        // Vérifie si la case est prise par un joueur
        if(hasMarkO() || hasMarkX()) {
            return true;
        }else return false;
       
    }
    
}
