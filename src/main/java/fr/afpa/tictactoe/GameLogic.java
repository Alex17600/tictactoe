package fr.afpa.tictactoe;

/**
 * Classe encapsulation les attributs et méthodes nécessaires à la logique du jeu.
 */
public class GameLogic {
    /**
     * Le plateau de jeu, passé en paramètre du constructeur.
     */
    private final BoardSquare[][] board;

    public GameLogic(BoardSquare[][] board) {
        this.board = board;
    }

    /** 
     * Vérifie si le joueur jouant les croix a gagné.
     *
     * */
    public boolean isWinnerX() {
        // Vérifier les lignes
        for (int i = 0; i < 3; i++) {
            if (board[i][0].hasMarkX() && board[i][1].hasMarkX() && board[i][2].hasMarkX()) {
                return true;
            }
        }
        // Vérifier les colonnes
        for (int j = 0; j < 3; j++) {
            if (board[0][j].hasMarkX() && board[1][j].hasMarkX() && board[2][j].hasMarkX()) {
                return true;
            }
        }
        // Vérifier les diagonales
        if (board[0][0].hasMarkX() && board[1][1].hasMarkX() && board[2][2].hasMarkX()) {
            return true;
        }
        if (board[0][2].hasMarkX() && board[1][1].hasMarkX() && board[2][0].hasMarkX()) {
            return true;
        }
        return false;
    }

    /** 
     * Vérifie si le joueur jouant les cercles a gagné.
     *
     * */
    public boolean isWinnerO() {
        // Vérifier les lignes
        for (int i = 0; i < 3; i++) {
            if (board[i][0].hasMarkO() && board[i][1].hasMarkO() && board[i][2].hasMarkO()) {
                return true;
            }
        }
        // Vérifier les colonnes
        for (int j = 0; j < 3; j++) {
            if (board[0][j].hasMarkO() && board[1][j].hasMarkO() && board[2][j].hasMarkO()) {
                return true;
            }
        }
        // Vérifier les diagonales
        if (board[0][0].hasMarkO() && board[1][1].hasMarkO() && board[2][2].hasMarkO()) {
            return true;
        }
        if (board[0][2].hasMarkO() && board[1][1].hasMarkO() && board[2][0].hasMarkO()) {
            return true;
        }
        return false;
    }

    /**
     * Vérifie si il y a encore une case disponble.
     * @return {@code true} Si case encore disponible.
     */
    public boolean hasGap() {
        // Vérifie si chaque case est prise par un joueur
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (!board[i][j].isTaken()) {
                    return true;
                }
            }
        }
        return false;
    }
}
